fn main() {
    println!("Hello, world!");
}

fn say_hello(name: &String) -> String {
    let mut greeting = String::from("Hello ");
    greeting.push_str(name);
    greeting.push_str("!");
    greeting
}







#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_say_hello() {
        let name = String::from("Bob");
        let answer = String::from("Hello Bob!");
        assert_eq!(say_hello(&name), answer);
    }
}